import main from '../src/test3'

describe('Testing 3', () => {
  test('First test', () => {
    expect(main('super califrastilistico espiralidoso')).toBe('super califrastilist...')
  })
  test('Second test', () => {
    expect(main('El futuro es hoy!!')).toBe('El futuro es hoy!!')
  })
  test('Third test', () => {
    expect(main('Se que no se nada :c')).toBe('Se que no se nada :c')
  })
  test('Fourth test', () => {
    expect(main('No me digas que no, no me digas adios')).toBe('No me digas que no, ...')
  })
})
