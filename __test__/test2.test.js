import main from '../src/test2'

describe('Testing 2', () => {
  test('First test', () => {
    expect(main(10)).toBeTruthy()
  })
  test('Second test', () => {
    expect(main(103)).toBeFalsy()
  })
  test('Third test', () => {
    expect(main(-4)).toBeTruthy()
  })
  test('Fourth test', () => {
    expect(main(-23)).toBeFalsy()
  })
})
