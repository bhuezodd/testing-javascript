import main from '../src/test4'

describe('Testing 4', () => {
  test('First test', () => {
    expect(main(10, 5)).toBe(10)
  })
  test('Second test', () => {
    expect(main(25, 4)).toBe(28)
  })
  test('Third test', () => {
    expect(main(6.6, 3.3)).toBe(6.6)
  })
  test('Fourth test', () => {
    expect(main(20.5, 1.5)).toBe(22.5)
  })
})
