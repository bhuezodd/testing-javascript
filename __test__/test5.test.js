import {helper, main} from '../src/test5'

describe('Testing 5 - First Test', () => {
  const a = helper(100)
  const b = main(a, 100)
  test('Validation type', () => {
    expect(a).toBeTruthy()
  })
  test('Validation result', () => {
    expect(b).toBe(100)
  })
})

describe('Testing 5 - Second Test', () => {
  const a = helper(20.12)
  const b = main(a, 20.12)
  test('Validation type', () => {
    expect(a).toBeTruthy()
  })
  test('Validation result', () => {
    expect(b).toBe('Formato no válido')
  })
})

describe('Testing 5 - Third Test', () => {
  const a = helper('20.0')
  const b = main(a, '20.0')
  test('Validation type', () => {
    expect(a).toBeFalsy()
  })
  test('Validation result', () => {
    expect(b).toBe(0)
  })
})

describe('Testing 5 - Fourth Test', () => {
  const a = helper('Pedro')
  const b = main(a, 'Pedro')
  test('Validation type', () => {
    expect(a).toBeFalsy()
  })
  test('Validation result', () => {
    expect(b).toBe(0)
  })
})

