import main from '../src/test1'

describe('Testing 1', () => {
  test('First test', () => {
    expect(main([1, 2, 3, 4])).toBe(4)
  })
  test('Second test', () => {
    expect(main([100, 23, 130, 67, 12])).toBe(130)
  })
  test('Third test', () => {
    expect(main([-2, -90, -22, -22, -93])).toBe(-2)
  })
  test('Fourth test', () => {
    expect(main([80, -80, -2, 0, 93])).toBe(93)
  })
})
