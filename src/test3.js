/*
 * Verficar que le tamaño del string sea menor a 20
 * En caso se mayor reducir a 20 y agregar '...' al final del string
 */

/*
 *  TEST:
 *  |     10       |  |     true     |
 *  |     103      |  |     false    |
 *  |      -4      |  |     true     |
 *
 */

function main(input) {
  return 0
}

export default main
