/*
 * Crear helper qeu valide que si es number devuelva true en caso sea otro tipo devuevla false
 * 
 * La funcion main resive dos argumentos a que es boolean y b que es el input
 * en caso a sea false devolver 0
 *
 * caso contrario validar si b tiene decimales
 * en caso contenga decimales devolver 'Formato no valido'
 * caso contrario devolver b
 */

/*
 *  TEST:
 *  |       100       |  |              100           |
 *  |      20.12      |  |     'Formato no válido'    |
 *  |      '20.0'     |  |               0            | 
 */

export const helper = (input) => {
  return 0
}

export const main = (a, b) => {
  return 0
}
