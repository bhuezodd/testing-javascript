/*
 * Se pasa como parametro a y b
 * Validar si a es divisible entre b en cuyo caso devolver a
 * Caso contrario devolver el numero mas cercano del que sea divisble b
 */

/*
 *  TEST:
 *  |     10, 5       |  |     10     |
 *  |     25, 4       |  |     28     |
 *  |    6.6, 3.3     |  |    6.6     | 
 */

function main(a, b) {
  return 0
}

export default main
